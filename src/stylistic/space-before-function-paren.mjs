/**
 * It allows to find declaration without IDE. Without this rule <kbd>Ctrl</kbd> + <kbd>F</kbd> would
 * find usages and not only definition
 */
export default {}
